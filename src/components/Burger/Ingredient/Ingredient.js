import React from 'react';
import './Ingredient.css';

const Ingredient = ({type}) => {
  switch (type){
    case 'bread-bottom':
      return <div className="BreadBottom"/>;
    case 'bread-top':
      return <div className="BreadTop"/>;
    case 'meat':
      return <div className="Meat"/>;
    case 'cheese':
      return <div className="Cheese"/>;
    case 'salad':
      return <div className="Salad"/>;
    case 'bacon':
      return <div className="Bacon"/>;
    default:
      return <div>No such ingredient</div>;
  }
};

export default Ingredient;