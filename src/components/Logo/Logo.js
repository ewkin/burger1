import React from 'react';
import './Logo.css';
import burger_logo from '../../assets/burger_logo.png';

const Logo = () => (
        <div className="Logo">
            <img src={burger_logo} alt="My Burger"/>

        </div>
    );

export default Logo;