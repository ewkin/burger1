import axios from 'axios'

const axiosOrders = axios.create({
    baseURL: 'https://js9-burger-kim-default-rtdb.firebaseio.com/'
});

axiosOrders.interceptors.request.use(req => {
    console.log('[In request interceptor]', req);
    return req;
});

axiosOrders.interceptors.request.use(res => {
    console.log('[In response interceptor]', res);
    return res;
});

export default axiosOrders;